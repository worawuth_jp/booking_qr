const multer = require("multer");

class UploadImage{
    getUpload(){
        const upload = multer({
            dest: "/path/to/temporary/directory/to/store/uploaded/files"
            // you might also want to set some limits: https://github.com/expressjs/multer#limits
          });
        return upload
    }
}

module.exports = new UploadImage();
const HTTP = require("../constants/http");
const MSG = require("../constants/message");

class SystemError extends Error{
    constructor(http,code,msg,stack){
        super(http,code,msg,stack);
        this.http = http;
        this.code = code;
        this.msg = msg;
        this.stack = stack;
        this.error = this.generateClientError(this.http,this.code,this.msg,this.stack);
        this.responseCode = this.error.responseCode;
        this.data = this.error.data;
    }

    generateClientError(http,code,msg,stack){
        let error = {};
        switch(http){
            case HTTP.SYSTEM_TIMEOUT_ERR_CODE :
                error = {
                    responseCode: HTTP.SYSTEM_TIMEOUT_ERR_CODE,
                    data: {
                        message: msg ? msg : MSG.SYSTEM_ERROR_TIME_OUT_MSG
                    },
                    code: code ? code : HTTP.SYSTEM_TIMEOUT_ERR_CODE,
                    mesage: MSG.SYSTEM_ERROR_TIME_OUT_MSG,
                    stack: stack
                }
                break;
            default :
                error = {
                    responseCode : HTTP.SYSTEM_ERR_HTTP_CODE,
                    data : {
                        message: msg ? msg: MSG.SYSTEM_ERROR_HTTP_MSG
                    },
                    code: code ? code : HTTP.SYSTEM_ERR_HTTP_CODE,
                    mesage: MSG.SYSTEM_ERROR_HTTP_MSG,
                    stack: stack
                }
        }
        return error;
    }
}

module.exports = SystemError;
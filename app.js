const createError = require("http-errors");
const httpContext = require("express-http-context");
const express = require("express");
const path = require("path");
const app = express();
const cors = require("cors");

const indexRouter = require("./routes/index");
const authRouter = require("./routes/auth");
const userRouter = require("./routes/user");
const tableRouter = require("./routes/tables");
const foodRouter = require("./routes/food");
const orderRouter = require("./routes/orders");
const corsOptions = {
  origin: true,
};

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(cors(corsOptions));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, "public")));
app.use(httpContext.middleware);

app.use("/", indexRouter);
app.use("/api", authRouter);
app.use("/api", userRouter);
app.use("/api", tableRouter);
app.use("/api", foodRouter);
app.use("/api", orderRouter);

app.use((err, req, res, next) => {
  next(createError(404));
});

app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = err;

  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;

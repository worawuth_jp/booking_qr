const express = require("express");
const router = express.Router();

const path = require("path");
const AuthEventRouter = require("./eventRouters/AuthEventRouter");

router.post("/login", (req, res) => {
  AuthEventRouter.authentication(req,res);
});

module.exports = router;
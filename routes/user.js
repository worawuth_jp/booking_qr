const express = require("express");
const UploadImage = require("../Api/UploadImage");
const userEventRouter = require("./eventRouters/UserEventRouter");
const router = express.Router();
const upload = UploadImage.getUpload();

router.get("/users", (req, res) => {
  userEventRouter.listUsers(req, res);
});

router.post("/users", upload.single("img"), (req, res) => {
  userEventRouter.addUser(req, res);
});

router.put("/users", upload.single("img"), (req, res) => {
  userEventRouter.editUser(req, res);
});

router.delete("/users", (req, res) => {
  userEventRouter.deleteUser(req, res);
});

module.exports = router;

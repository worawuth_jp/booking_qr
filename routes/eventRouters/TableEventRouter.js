const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const TableController = require("../../controllers/TableController");

class TableEventRouter{
    async listTables(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            let successWithResult ;
            if(body.table_id){
                successWithResult = await TableController.listTablesById(body);
            }
            else{
                successWithResult = await TableController.listTables();
            }
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('TableEventRouter.listTables() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async addTable(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            const body = req.body;
            let successWithResult ;
            successWithResult = await TableController.addTable(body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('TableEventRouter.addTable() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async editTable(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            const body = req.body;
            let successWithResult ;
            successWithResult = await TableController.editTable(body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('TableEventRouter.addTable() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async deleteTable(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            let successWithResult ;
            successWithResult = await TableController.deleteTable(body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('TableEventRouter.deleteTable() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }
}

module.exports = new TableEventRouter();
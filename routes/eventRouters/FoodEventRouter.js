const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const FoodController = require("../../controllers/FoodController");

class FoodEventRouter{
    async listFoods(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            let successWithResult ;
            if(body.food_id){
                successWithResult = await FoodController.listFoodById(body);
            }
            else{
                successWithResult = await FoodController.listFoods();
            }
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('FoodEventRouter.listFoods() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async addFood(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            const body = req.body;
            const file = req.file;
            let successWithResult = await FoodController.addFood(body,file);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('FoodEventRouter.addFood() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async editFood(req,res){
        try {
            console.log('ParamsRequest --> ',req.body);
            const body = req.body;
            const file = req.file;
            let successWithResult = await FoodController.editFood(body,file);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('FoodEventRouter.editFood() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async deleteFood(req,res){
        try {
            console.log('ParamsRequest --> ',req.query);
            const body = req.query;
            let successWithResult = await FoodController.deleteFood(body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);
        } catch (error) {
            console.error('FoodEventRouter.deleteFood() Exception ---> ',error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

}

module.exports = new FoodEventRouter();
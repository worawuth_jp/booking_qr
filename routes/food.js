const express = require("express");
const UploadImage = require("../Api/UploadImage");
const FoodEventRouter = require("./eventRouters/FoodEventRouter");
const upload = UploadImage.getUpload();

const router = express.Router();

router.get("/foods", (req, res) => {
  FoodEventRouter.listFoods(req, res);
});

router.post("/foods", upload.single("food_img"), (req, res) => {
  FoodEventRouter.addFood(req, res);
});

router.put("/foods", upload.single("food_img"), (req, res) => {
    FoodEventRouter.editFood(req, res);
});

router.delete("/foods", (req, res) => {
    FoodEventRouter.deleteFood(req, res);
});

module.exports = router;

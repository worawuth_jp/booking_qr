const express = require("express");
const TableEventRouter = require("./eventRouters/TableEventRouter");
const router = express.Router();

router.get("/tables", (req, res) => {
  TableEventRouter.listTables(req, res);
});

router.post("/tables", (req, res) => {
    TableEventRouter.addTable(req, res);
});

router.put("/tables", (req, res) => {
  TableEventRouter.editTable(req, res);
});

router.delete("/tables", (req, res) => {
  TableEventRouter.deleteTable(req, res);
});

module.exports = router;
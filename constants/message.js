module.exports = {
    SUCCESS_MESSAGE: 'SUCCESS',
    SUCCESS_INSERT_MESSAGE: 'Insert Success',
    SUCCESS_UPDATE_MESSAGE: 'Update Success',
    SUCCESS_DELETE_MESSAGE: 'Delete Success',
    SUCCESS_UPLOAD_MESSAGE: 'Upload Success',
    SUCCESS_FILE_MESSAGE: 'File Move Success',

    FAILURE_INSERT_MESSAGE: 'Insert Failure',
    FAILURE_UPDATE_MESSAGE: 'Update Failure',
    FAILURE_DELETE_MESSAGE: 'Delete Failure',
    FAILURE_UPLOAD_MESSAGE: 'Upload Failure',
    
    ERROR_MESSAGE: 'ERROR',

    CLIENT_ERROR_BAD_REQUEST_MSG : 'ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง',
    CLIENT_ERROR_NOT_FOUND_MSG: 'ไม่พบ api',
    CLIENT_ERROR_UNAUTHENTICATION_MSG : 'login failure',
    USERNAME_DUP_ERROR_MSG: 'username ถูกใช้งานแล้ว',

    SYSTEM_ERROR_TIME_OUT_MSG: 'Time Out Error',
    SYSTEM_ERROR_HTTP_MSG: 'Internal Server Error'

}
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const AuthenticationService = require("../services/AuthenticationService");

class AuthController {
    async authentication(body){
        try {
            const username = body.username;
            const password = body.password;
            const resultSuccess = await AuthenticationService.login(username,password)
            return resultSuccess;
        } catch (error) {
            console.error('AuthController.authentication() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }
}

module.exports = new AuthController();
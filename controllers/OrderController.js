const http = require("../constants/http");
const message = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const path = require("path");
const OrderService = require("../services/OrderService");

class OrderController{
    async listOrders(){
        try {
            const resultSuccess = await OrderService.listOrders();
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.listOrders() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async listOrderById(params){
        try {
            const isUndefined = params.order_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const resultSuccess = await OrderService.listOrderById(params.order_id);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.listOrderById() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async addOrder(body,file){
        try {
            const isUndefined = body.table_id && body.member_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const orderCode = ``;
            const tableId = body.table_id;
            const memberId = body.memberId;
            let imgPath,tempPath,targetPath;
            if(file){
                tempPath = file.path;
                const random = Math.ceil(Math.random() * 100000);
                imgPath = `images/foods/food_${random}_${Date.now()}.png`;
                targetPath = path.join(__dirname, `../public/${imgPath}`);
                console.log('Target Path',targetPath);
            }
            const resultSuccess = await OrderService.addOrder(orderCode,tableId,memberId);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.addOrder() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async editOrder(body,file){
        try {
            const isUndefined = body.food_name && body.food_price && body.food_detail && body.food_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const foodName = body.food_name;
            const foodPrice = body.food_price;
            const foodDetail = body.food_detail;
            const foodId = body.food_id;
            let imgPath,tempPath,targetPath;
            if(file){
                tempPath = file.path;
                const random = Math.ceil(Math.random() * 100000);
                imgPath = `images/foods/food_${random}_${Date.now()}.png`;
                targetPath = path.join(__dirname, `../public/${imgPath}`);
                console.log('Target Path',targetPath);
            }
            const resultSuccess = await OrderService.editFood(foodId,foodName,foodPrice,foodDetail,imgPath,targetPath,tempPath);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.editOrder() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteOrder(body){
        try {
            const isUndefined = body.food_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const foodId = body.food_id;
            
            const resultSuccess = await OrderService.deleteFood(foodId);
            return resultSuccess;
        } catch (error) {
            console.error('OrderController.deleteOrder() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

}

module.exports = new OrderController()
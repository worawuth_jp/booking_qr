const http = require("../constants/http");
const message = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const FoodService = require("../services/FoodService");
const path = require("path");

class FoodController{
    async listFoods(){
        try {
            const resultSuccess = await FoodService.listFoods();
            return resultSuccess;
        } catch (error) {
            console.error('FoodController.listFoods() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async listFoodById(params){
        try {
            const isUndefined = params.food_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const resultSuccess = await FoodService.listFoodById(params.food_id);
            return resultSuccess;
        } catch (error) {
            console.error('FoodController.listFoods() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async addFood(body,file){
        try {
            const isUndefined = body.food_name && body.food_price && body.food_detail && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const foodName = body.food_name;
            const foodPrice = body.food_price;
            const foodDetail = body.food_detail;
            let imgPath,tempPath,targetPath;
            if(file){
                tempPath = file.path;
                const random = Math.ceil(Math.random() * 100000);
                imgPath = `images/foods/food_${random}_${Date.now()}.png`;
                targetPath = path.join(__dirname, `../public/${imgPath}`);
                console.log('Target Path',targetPath);
            }
            const resultSuccess = await FoodService.addFood(foodName,foodPrice,foodDetail,imgPath,targetPath,tempPath);
            return resultSuccess;
        } catch (error) {
            console.error('FoodController.addFood() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async editFood(body,file){
        try {
            const isUndefined = body.food_name && body.food_price && body.food_detail && body.food_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const foodName = body.food_name;
            const foodPrice = body.food_price;
            const foodDetail = body.food_detail;
            const foodId = body.food_id;
            let imgPath,tempPath,targetPath;
            if(file){
                tempPath = file.path;
                const random = Math.ceil(Math.random() * 100000);
                imgPath = `images/foods/food_${random}_${Date.now()}.png`;
                targetPath = path.join(__dirname, `../public/${imgPath}`);
                console.log('Target Path',targetPath);
            }
            const resultSuccess = await FoodService.editFood(foodId,foodName,foodPrice,foodDetail,imgPath,targetPath,tempPath);
            return resultSuccess;
        } catch (error) {
            console.error('FoodController.editFood() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteFood(body){
        try {
            const isUndefined = body.food_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const foodId = body.food_id;
            
            const resultSuccess = await FoodService.deleteFood(foodId);
            return resultSuccess;
        } catch (error) {
            console.error('FoodController.deleteFood() Exception --> ',error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

}

module.exports = new FoodController()
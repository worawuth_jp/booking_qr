const DBAPI = require("../Api/DBApi");
const TransformModelUtils = require("../utils/TransformModelUtils");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const fs = require('fs');
const errors = require("../constants/errors");

class UserService{
    async getUsers(id=null){
        const DBApi = new DBAPI();
        try {
            let queryParams = [];
            let sql = `SELECT MEMBER.member_id,MEMBER.member_firstname,MEMBER.member_lastname,MEMBER.member_username,MEMBER.member_img,ROLE.member_role_id,ROLE.member_role_title
            FROM members MEMBER
            INNER JOIN member_roles ROLE ON ROLE.member_role_id = MEMBER.member_role_id
            WHERE 1`;
            if(id){
                sql += ` AND MEMBER.member_id=?`;
                queryParams.push(id)
            }
            const resultSet = await DBApi.query(sql,queryParams);
            console.log('QUERY DATA ---> ',resultSet);
            let data = {};
            if(id){
                if(resultSet.length !== 0){
                    data = resultSet[0];
                }
            }

            return await TransformModelUtils.transformResponseWithDataModel(data);
            
        } catch (error) {
            console.error("UserService.getUsers() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        } finally{
            DBApi.closeConnection();
        }
    }

    async addUser(firstname,lastname,username,password,role_id,tempPath,targetPath,imgPath){
        const DBApi = new DBAPI();
        try {
            let result;
            if(imgPath){
                result = await new Promise((resolve,reject)=>{
                    fs.rename(tempPath, targetPath, err => {
                        if(err) reject(false);
    
                        resolve(true);
                    })
                });

                console.log('Result ---> ',result);
                if(!result){
                    throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
                }
            }

            
            let queryParams = [];
            let sql = `INSERT INTO members (member_firstname,member_lastname, member_username, member_password, member_img, member_role_id)
            VALUES(?,?,?,?,?,?)`;
            queryParams.push(firstname,lastname,username,password,imgPath,role_id)
            const resultSet = await DBApi.query(sql,queryParams);
            console.log('QUERY DATA ---> ',resultSet);
            let data = {};
            
            if(resultSet){
                data = {
                    msg:MSG.SUCCESS_INSERT_MESSAGE,
                    isInsert: true
                }
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
            
        } catch (error) {
            console.error("UserService.addUser() Exception --> ", error);
            if(error.code && error.code === errors.ERR_DUP_SQL){
                console.log('DUPLICATE USERNAME')
                throw new ClientError(HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,MSG.USERNAME_DUP_ERROR_MSG)
            }
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        } finally{
            DBApi.closeConnection();
        }
    }

    async editUser(memberId,firstname,lastname,username,password,role_id,tempPath,targetPath,imgPath){
        const DBApi = new DBAPI();
        try {

            let result;
            if(imgPath){
                result = await new Promise((resolve,reject)=>{
                    fs.rename(tempPath, targetPath, err => {
                        if(err) reject(false);
    
                        resolve(true);
                    })
                });

                console.log('Result ---> ',result);
                if(!result){
                    throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
                }
            }

            let queryParams = [];
            let sql = `UPDATE members 
            SET member_firstname=?,member_lastname=?,member_username=?,member_password=?,member_role_id=? `;
            queryParams.push(firstname,lastname,username,password,role_id);

            if(imgPath){
                sql += `,member_img=?`;
                queryParams.push(imgPath);
            }

            sql += ` WHERE member_id=?`;
            queryParams.push(memberId);
            const resultSet = await DBApi.query(sql,queryParams);
            console.log('QUERY DATA ---> ',resultSet);
            let data = {};
            
            if(resultSet){
                data = {
                    msg:MSG.SUCCESS_UPDATE_MESSAGE,
                    isEdit: true
                }
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
            
        } catch (error) {
            console.error("UserService.editUser() Exception --> ", error);
            if(error.code && error.code === errors.ERR_DUP_SQL){
                console.log('DUPLICATE USERNAME')
                throw new ClientError(HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,MSG.USERNAME_DUP_ERROR_MSG)
            }
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        } finally{
            DBApi.closeConnection();
        }
    }

    async deleteUser(memberId){
        const DBApi = new DBAPI();
        try {
            let queryParams = [];
            let sql = `DELETE FROM members WHERE member_id=?`;
            queryParams.push(memberId);
            const resultSet = await DBApi.query(sql,queryParams);
            console.log('QUERY DATA ---> ',resultSet);
            let data = {};
            
            if(resultSet){
                data = {
                    msg:MSG.SUCCESS_DELETE_MESSAGE,
                    isDelete: true
                }
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
            
        } catch (error) {
            console.error("UserService.deleteUser() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        } finally{
            DBApi.closeConnection();
        }
    }
}

module.exports = new UserService();
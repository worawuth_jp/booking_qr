const DBAPI = require("../Api/DBApi");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const TransformModelUtils = require("../utils/TransformModelUtils");
const fs = require('fs');
const { COMING_STATUS, NOT_PAY_STATUS, PAY_STATUS } = require("../constants/status");

class OrderService{
    async listOrders(){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT order_id,order_code,table_id,member_id,status FROM orders`;
            const resultSet = await DBApi.query(sql)
            for await (const data of resultSet){
                let query = `SELECT 
                ORDERFOOD.order_food_id,
                ORDERFOOD.food_material_id,
                ORDERFOOD.order_num,
                ORDERFOOD.order_amount,
                MATERIAL.food_material_price,
                FOOD.food_name,
                FOOD.food_price,
                FOOD.food_detail,
                FOOD.food_img,
                FOOD.isEnable
                FROM order_foods ORDERFOOD
                INNER JOIN food_materials MATERIAL ON MATERIAL.food_material_id = ORDERFOOD.food_material_id 
                INNER JOIN foods FOOD ON FOOD.food_id = MATERIAL.food_id
                WHERE ORDERFOOD.order_id=?`
                let params = [data.order_id];
                data.order_foods = await DBApi.query(query,params);
                data.isEnable = data.isEnable ? true : false;
                data.status = data.status == 0 ? COMING_STATUS : data.status == 1 ? NOT_PAY_STATUS : data.status == 2 ? PAY_STATUS : '';
            }
            return await TransformModelUtils.transformResponseWithDataModel(resultSet);
        } catch (error) {
            console.error("OrderService.listOrders() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async listOrderById(id){
        const DBApi = new DBAPI();
        try {
            let sql = `SELECT order_id,order_code,table_id,member_id,status FROM orders WHERE order_id=?`;
            let queryParams = [id];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let result = {};
            for await (const data of resultSet){
                let query = `SELECT 
                ORDERFOOD.order_food_id,
                ORDERFOOD.food_material_id,
                ORDERFOOD.order_num,
                ORDERFOOD.order_amount,
                MATERIAL.food_material_price,
                FOOD.food_name,
                FOOD.food_price,
                FOOD.food_detail,
                FOOD.food_img,
                FOOD.isEnable
                FROM order_foods ORDERFOOD
                INNER JOIN food_materials MATERIAL ON MATERIAL.food_material_id = ORDERFOOD.food_material_id 
                INNER JOIN foods FOOD ON FOOD.food_id = MATERIAL.food_id
                WHERE ORDERFOOD.order_id=?`
                let params = [data.order_id];
                data.order_foods = await DBApi.query(query,params);
                data.isEnable = data.isEnable ? true : false;
                data.status = data.status == 0 ? COMING_STATUS : data.status == 1 ? NOT_PAY_STATUS : data.status == 2 ? PAY_STATUS : '';
                result = data;
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("OrderService.listOrderById() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async addOrder(orderCode,tableId,memberId){
        const DBApi = new DBAPI();
        try {

            let result;
            if(imgPath){
                result = await new Promise((resolve,reject)=>{
                    fs.rename(tempPath, targetPath, err => {
                        if(err) reject(false);
    
                        resolve(true);
                    })
                });

                console.log('Result ---> ',result);
                if(!result){
                    throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
                }
            }

            let sql = `INSERT INTO orders (order_code,table_id,member_id) VALUES(?,?,?)`;
            let queryParams = [orderCode,tableId,memberId];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let data = {
                msg: MSG.SUCCESS_INSERT_MESSAGE,
                isInsert: true,
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_INSERT_MESSAGE,
                    isInsert: false,
                };
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.addOrder() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async editOrder(foodId,foodName,foodPrice,foodDetail,imgPath,targetPath,tempPath){
        const DBApi = new DBAPI();
        try {

            let result;
            if(imgPath){
                result = await new Promise((resolve,reject)=>{
                    fs.rename(tempPath, targetPath, err => {
                        if(err) reject(false);
    
                        resolve(true);
                    })
                });

                console.log('Result ---> ',result);
                if(!result){
                    throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
                }
            }

            let sql = `UPDATE foods SET food_name=?,food_price=?,food_detail=?`;
            let queryParams = [foodName,foodPrice,foodDetail];
            if(imgPath){
                sql += ` ,food_img=?`;
                queryParams.push(imgPath);
            }
            const resultSet = await DBApi.query(sql,queryParams)
            
            let data = {
                msg: MSG.SUCCESS_UPDATE_MESSAGE,
                isEdit: true,
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_UPDATE_MESSAGE,
                    isEdit: false,
                };
            }
            return await TransformModelUtils.transformResponseWithDataModel(data);
        } catch (error) {
            console.error("OrderService.editOrder() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }

    async deleteOrder(foodId){
        const DBApi = new DBAPI();
        try {
            let sql = `DELETE FROM foods WHERE food_id=?`;
            let queryParams = [foodId];
            const resultSet = await DBApi.query(sql,queryParams)
            
            let result = {
                msg:MSG.SUCCESS_DELETE_MESSAGE,
                isDelete: true
            };
            if(!resultSet){
                result = {
                    msg:MSG.FAILURE_DELETE_MESSAGE,
                    isDelete: false
                };
            }

            console.log('RESULT ---> ',result);
            return await TransformModelUtils.transformResponseWithDataModel(result);
        } catch (error) {
            console.error("OrderService.deleteOrder() Exception --> ", error);
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }
        finally{
            DBApi.closeConnection();
        }
    }
    
}

module.exports = new OrderService();